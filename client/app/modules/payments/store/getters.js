export function payments(state) {
  return state.rows;
}

export function selected(state) {
  return state.selected;
}