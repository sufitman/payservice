export const LOAD 			= "LOAD";
export const UPDATE 		= "UPDATE";
export const ADD 		    = "ADD";
export const SELECT			= "SELECT";
export const CLEAR_SELECT	= "CLEAR_SELECT";