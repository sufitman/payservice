import toastr from '../../../core/toastr';
import { ADD, LOAD, SELECT, CLEAR_SELECT, UPDATE } from './types';
import axios from 'axios';
import { map, cloneDeep } from 'lodash';

export const NAMESPACE = '/api/payments';

export const selectRow = ({ commit }, row, multiSelect) => {
  commit(SELECT, row, multiSelect);
};

export const clearSelection = ({ commit }) => {
  commit(CLEAR_SELECT);
};

export const getPayments = ({ commit }, callback) => {
  axios.get(NAMESPACE).then(callback).catch((response) => {
    console.error('Request error!', response.statusText);
  });
};

export const downloadRows = ({ commit }) => {
  getPayments({ commit }, (response) => {
    let res = response.data;
    if (res.status === 200 && res.data)
      commit(LOAD, res.data);
    else
      console.error('Request error!', res.error);

  });
};

export const created = ({ commit }, row, needSelect) => {
  commit(ADD, row);

  if (needSelect) {
    commit(SELECT,row, true);
  }
};


export const updated = ({ commit }, row) => {
  commit(UPDATE, row);
};