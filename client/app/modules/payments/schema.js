import Vue from 'vue';

let _ = Vue.prototype._;

module.exports = {
  id: 'payments',
  title: _('Payments'),
  table: {
    multiSelect: true,
    columns: [
      {
        title: _('Name'),
        field: 'name',
        align: 'left'
      },
      {
        title: _('Email'),
        field: 'email'
      },
      {
        title: _('Product'),
        field: 'product'
      },
      {
        title: _('Amount'),
        field: 'amount'
      },
      {
        title: _('IsMonthly'),
        field: 'isMonthly'
      },
      {
        title: _('UpdatedAt'),
        field: 'updatedAt'
      }
    ],

    rowClasses: function(model) {
      return {

      };
    }
  },

  options: {
    searchable: true,
    enableNewButton: false,
    enabledSaveButton: false,
    enableDeleteButton: false,
    enableCloneButton: false,

    validateAfterLoad: false, // Validate after load a model
    validateAfterChanged: false, // Validate after every changes on the model
    validateBeforeSave: true, // Validate before save a model
    hasOwner: true,
    ownerReference: 'userCode'
  },

  events: {
    onSelect: null
  },

  resources: {

  }
};