import toastr from '../../../core/toastr';
import { LOAD, ADD, SELECT, CLEAR_SELECT, UPDATE /*, REMOVE*/ } from './types';
import axios from 'axios';
import { map, cloneDeep } from 'lodash';

export const NAMESPACE = '/api/products';

export const selectRow = ({ commit }, row, multiSelect) => {
  commit(SELECT, row, multiSelect);
};

export const clearSelection = ({ commit }) => {
  commit(CLEAR_SELECT);
};

export const getProducts = ({ commit }, callback) => {
  axios.get(NAMESPACE).then(callback).catch((response) => {
    console.error('Request error!', response.statusText);
  });
};

export const downloadRows = ({ commit }) => {
  getProducts({ commit }, (response) => {
    let res = response.data;
    if (res.status === 200 && res.data)
      commit(LOAD, res.data);
    else
      console.error('Request error!', res.error);

  });
};

export const saveRow = ({ commit }, model) => {
  axios.post(NAMESPACE, model).then((response) => {
    let res = response.data;
    if (res.status === 200 && res.data) {
      created({ commit }, res.data, true);
    }
  }).catch((response) => {
    toastr.error(response);
  });		
};

export const created = ({ commit }, row, needSelect) => {
  commit(ADD, row);

  if (needSelect) {
    commit(SELECT,row, true);
  }
};

export const updateRow = ({ commit }, row) => {
  axios.put(NAMESPACE + '/' + row.code, row).then((response) => {
    let res = response.data;
    if (res.data) {
      commit(UPDATE, res.data);
      commit(CLEAR_SELECT);
    }
  }).catch((response) => {
    toastr.error(response);
  });	
};

export const updated = ({ commit }, row) => {
  commit(UPDATE, row);
};

// export const removeRow = ({ commit }, row) => {
//   axios.delete(NAMESPACE + '/' + row.code).then((response) => {
//     commit(REMOVE, row);
//   }).catch((response) => {
//     toastr.error(response);
//   });
// };
//
// export const removed = ({ commit }, row) => {
//   commit(REMOVE, row);
// };
