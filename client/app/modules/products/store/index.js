import { LOAD, ADD, SELECT, CLEAR_SELECT, UPDATE /*, REMOVE*/ } from './types';

import { each, find, assign, /*remove,*/ isArray } from 'lodash';

let fixModel = (row) => {
  // Do something if necessary
};

const state = {
  rows: [],
  selected: []
};

const mutations = {
  [LOAD] (state, models) {
    each(models, model => fixModel(model));
    state.rows.splice(0);
    state.rows.push(...models);
  },

  [ADD] (state, model) {
    let found = find(state.rows, (item) => item.code === model.code);
    if (!found) {
      fixModel(model)
      state.rows.push(model);
    }
  },

  [SELECT] (state, row, multiSelect) {
    if (isArray(row)) {
      each(row, model => fixModel(model));
      state.selected.splice(0);
      state.selected.push(...row);
    } else {
      fixModel(row);

      if (multiSelect === true && state.selected.indexOf(row) !== -1) {
        state.selected.splice(state.selected.indexOf(row), 1);
      } else {
        state.selected.splice(0);
      }
      state.selected.push(row);
    }
  },

  [CLEAR_SELECT] (state) {
    state.selected.splice(0);
  },

  [UPDATE] (state, model) {
    each(state.rows, (item) => {
      if (item.code === model.code) {
        assign(item, model);
        fixModel(item);
      }
    });
  },

  // [REMOVE] (state, model) {
  //   state.rows = state.rows.filter(item => item.code !== model.code);
  // }	
};

import * as getters from './getters';
import * as actions from './actions';

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};