export function products(state) {
  return state.rows;
}

export function selected(state) {
  return state.selected;
}