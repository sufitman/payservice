import Vue from 'vue';
import { validators } from 'vue-form-generator';

let _ = Vue.prototype._;

module.exports = {
  id: 'products',
  title: _('Products'),
  table: {
    multiSelect: true,
    columns: [
      {
        title: _('Name'),
        field: 'name',
        align: 'left'
      },
      {
        title: _('Price'),
        field: 'price'
      },
      {
        title: _('Currency'),
        field: 'currency'
      },
      {
        title: _('Description'),
        field: 'description'
      }
    ],

    rowClasses: function(model) {
      return {
      };
    }

  },

  form: {
    fields: [
      {
        type: 'input',
        inputType: 'text',
        label: _('ProductName'),
        model: 'name',
        required: true,
        placeholder: _('EnterProductName')
      },
      {
        type: 'input',
        inputType: 'number',
        label: _('Price'),
        model: 'price',
        required: true,
        placeholder: _('EnterPrice'),
        validator: validators.double
      },
      {
        type: 'select',
        label: _('Currency'),
        model: 'currency',
        required: true,
        values: ['USD', 'EUR']
      },
      {
        type: 'input',
        inputType: 'text',
        label: _('Description'),
        model: 'description',
        required: false,
        placeholder: _('EnterProductDescription')
      },
    ]
  },

  options: {
    searchable: true,
    enableNewButton: true,
    enabledSaveButton: true,
    enableDeleteButton: false,
    enableCloneButton: false,

    validateAfterLoad: false, // Validate after load a model
    validateAfterChanged: false, // Validate after every changes on the model
    validateBeforeSave: true, // Validate before save a model
    hasOwner: true,
    ownerReference: 'userCode'
  },

  events: {
    onSelect: null,
    onNewItem: null,
    onCloneItem: null,
    onSaveItem: null,
    onDeleteItem: null,
    onChangeItem: null,
    onValidated(model, errors, schema) {
      if (errors.length > 0)
        console.warn('Validation error in page! Errors:', errors, ', Model:', model);
    }
  },

  resources: {
    addCaption: _('AddNewProduct'),
    saveCaption: _('Save'),
    cloneCaption: _('Clone'),
    deleteCaption: _('Delete')
  }
};