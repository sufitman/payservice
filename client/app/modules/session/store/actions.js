import axios from 'axios';
import { SET_USER } from './types';

export const NAMESPACE= '/api/session';

export const getSessionUser = ({ commit }) => {
  axios.get(NAMESPACE + '/me').then((response) => {
    let res = response.data;
    if (res.status == 200) {
      commit(SET_USER, res.data);
    } else {
      console.error('Request error!', res.error);
    }

  }).catch((response) => {
    console.error('Request error!', response.statusText);
  });
};

