import { SET_USER } from './types';

const state = {
  user: null,
  notifications: [
    { id: 1, text: 'Something happened!', time: 1, user: null }
  ],
  messages: [],
  searchText: ''
};

const mutations = {

  [SET_USER] (state, user) {
    state.user = user;
  },

};

import * as getters from './getters';
import * as actions from './actions';

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};