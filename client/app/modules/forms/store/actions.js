import toastr from '../../../core/toastr';
import { LOAD, ADD, SELECT, CLEAR_SELECT, UPDATE, REMOVE } from './types';
import axios from 'axios';
import { map, cloneDeep } from 'lodash';

export const NAMESPACE = '/api/forms';

export const selectRow = ({ commit }, row, multiSelect) => {
  commit(SELECT, row, multiSelect);
};

export const clearSelection = ({ commit }) => {
  commit(CLEAR_SELECT);
};

export const downloadRows = ({ commit }) => {
  axios.get(NAMESPACE).then((response) => {
    let res = response.data;
    if (res.status == 200 && res.data)
      commit(LOAD, res.data);
    else
      console.error('Request error!', res.error);

  }).catch((response) => {
    console.error('Request error!', response.statusText);
  });

};

export const saveRow = ({ commit }, model) => {
  let postData = cloneDeep(model);
  postData.products = map(postData.products, 'code');
  axios.post(NAMESPACE, postData).then((response) => {
    let res = response.data;
    if (res.status === 200 && res.data) {
      created({ commit }, res.data, true);
    }
  }).catch((response) => {
    toastr.error(response);
  });		
};

export const created = ({ commit }, row, needSelect) => {
  commit(ADD, row);

  if (needSelect) {
    commit(SELECT,row, true);
  }
};

export const updateRow = ({ commit }, row) => {
  let postData = cloneDeep(row);
  postData.products = map(postData.products, 'code');
  axios.put(NAMESPACE + '/' + postData.code, postData).then((response) => {
    let res = response.data;
    if (res.data) {
      commit(UPDATE, res.data);
      commit(CLEAR_SELECT);
    }
  }).catch((response) => {
    toastr.error(response);
  });	
};

export const updated = ({ commit }, row) => {
  commit(UPDATE, row);
};

export const removeRow = ({ commit }, row) => {
  axios.delete(NAMESPACE + '/' + row.code).then((response) => {
    commit(REMOVE, row);
  }).catch((response) => {
    toastr.error(response);
  });
};

export const removed = ({ commit }, row) => {
  commit(REMOVE, row);
};
