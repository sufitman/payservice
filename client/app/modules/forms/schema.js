import Vue from 'vue';
import { validators } from 'vue-form-generator';

import { map } from 'lodash';
let _ = Vue.prototype._;

let config = require('../../../../package.json').clientConfig;
let url = 'http' + (config.isSecure ? 's' : '');
url += '://' + (config.serverHost || 'localhost');
url += ':' + (config.serverPort || 3000) + '/payment-form/';


module.exports = {
  id: 'forms',
  title: _('Forms'),
  table: {
    multiSelect: true,
    columns: [
      {
        title: _('Script'),
        field: 'code',
        align: 'left',
        formatter(value, model) {
          return model ? '&lt;script src="' + url + model.code + '"&gt;&lt;/script&gt;' : value;
        }
      },
      {
        title: _('Products'),
        field: 'products',
        formatter(value, model) {
          return map(model.products, 'name').join(', ');
        }
      },
    ],

    rowClasses: function(model) {
      return {
      };
    }

  },

  form: {
    fields: [
      {
        type: 'vueMultiSelect',
        label: _('Products'),
        placeholder: _('SelectProducts'),
        model: 'products',
        validator: validators.required,
        required: true,
        values: [],
        selectOptions: {
          multiple: true,
          hideSelected: true,
          closeOnSelect: true,
          key: 'code',
          label: 'name',
          searchable: true,
          showLabels: false,
          allowEmpty: true
        }
      },
      {
        type: 'input',
        inputType: 'number',
        label: _('FormWidth'),
        model: 'width',
        required: false,
        placeholder: _('EnterFormWidth'),
        validator: validators.integer
      },
      {
        type: 'select',
        label: _('FormWidthMeasure'),
        model: 'widthMeasure',
        required: false,
        values: ['px', 'pt', 'em', '%']
      },
      {
        type: 'input',
        inputType: 'number',
        label: _('FormHeight'),
        model: 'height',
        required: false,
        placeholder: _('EnterFormHeight'),
        validator: validators.integer
      },
      {
        type: 'select',
        label: _('FormHeightMeasure'),
        model: 'heightMeasure',
        required: false,
        values: ['px', 'pt', 'em', '%']
      }
    ]
  },

  options: {
    searchable: true,
    enableNewButton: true,
    enabledSaveButton: true,
    enableDeleteButton: true,
    enableCloneButton: false,

    validateAfterLoad: false, // Validate after load a model
    validateAfterChanged: false, // Validate after every changes on the model
    validateBeforeSave: true, // Validate before save a model
    hasOwner: true,
    ownerReference: 'userCode'
  },

  events: {
    onSelect: null,
    onNewItem: null,
    onCloneItem: null,
    onSaveItem: null,
    onDeleteItem: null,
    onChangeItem: null,
    onValidated(model, errors, schema) {
      if (errors.length > 0)
        console.warn('Validation error in page! Errors:', errors, ', Model:', model);
    }
  },

  resources: {
    addCaption: _('AddNewForm'),
    saveCaption: _('Save'),
    cloneCaption: _('Clone'),
    deleteCaption: _('Delete')
  }
};