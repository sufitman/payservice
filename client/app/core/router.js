import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '../modules/home';
import Forms from '../modules/forms';
import Products from '../modules/products';
import Payments from '../modules/payments';
import Settings from '../modules/settings';

Vue.use(VueRouter);

export default new VueRouter({
  mode: 'hash',
  routes: [
    { path: '/', component: Home },
    { path: '/forms', component: Forms },
    { path: '/products', component: Products },
    { path: '/settings', component: Settings },
    { path: '/payments', component: Payments }
  ]
});