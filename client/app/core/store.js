import Vue from 'vue';
import Vuex from 'vuex';

import session from '../modules/session/store';
import forms from '../modules/forms/store';
import products from '../modules/products/store';
import payments from '../modules/payments/store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    session,
    forms,
    products,
    payments
  }
});