import App from './core/App';
import Vue from 'vue';
import VueI18Next from '../app/core/i18next';

Vue.use(VueI18Next, (i18next) => {
  new Vue({
    el: '#app',
    components: {
      App
    }
  });
});