FROM node:carbon

WORKDIR /usr/src/app

COPY . .

ENTRYPOINT ./bin/entrypoint

EXPOSE 3000