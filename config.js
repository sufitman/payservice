module.exports = {
  // Secret for ID hashing
  hashSecret: 'CQwViwYM0z5L7VH7PzKPzxr4cW4rn2ARe6eVPIOtNIu',

  // Secret for session hashing
  sessionSecret: 'XpFjGA5PnPJE9xbjjRxpymlle5deUwCC8lRW8rKgQBN',

  // Application settings
  app: {
    //title: "VEM APP",
    //version: "1.0.0",
    //description: "This is my boilerplate web app",
    //keywords: "boilerplate, starter, webapp",
    //url: "http://localhost:3000/",
    //googleAnalyticsID: 'UA-xxxxx-x',
    //contactEmail: "hello@vem-app.com"
  },

  // Features of application
  features: {
    disableSignUp: false,
    verificationRequired: true
  },

  // Logging settings
  logging: {
    console: {
      // level: "debug"
    },

    file: {
      enabled: false
      // path: path.join(global.rootPath, "logs"),
      // level: "info",
      // json: false,
      // exceptionsSeparateFile: true
    }
  }
};

