A test project

Inspired by icebob's mevn-boilerplate (https://github.com/icebob/vue-express-mongo-boilerplate)

Before run make sure:
1) you have properly configured ".env" file in environments folder
(most of working variables are stored in "default.env" except for paypal rest api client id and secret token)
2) have Docker installed for running the app in container (via bash script ./init-docker) or mongodb, nodejs, npm 
(on debian-like linux os) for running in your local environment (via bash script ./init)

Run:
a) ./init or ./init-docker to run the app with webpack
b) "npm install; npm build; node server" to run without webpack

Undone yet: 
1) Paypall integration was not tested properly
2) EsLint fixes wasnot ended
3) Payment form wasnot styled
4) No test was written
5) Payments are not shown @ dashboard or separate route (insufficient)
6) Codestyle wasn`t reviewed 
7) Localization is messed