let path = require('path');
let pkg = require('../../package.json');
let config = pkg.config;
let url = 'http' + (config.isSecure ? 's' : '');
url += '://' + (config.serverHost || 'localhost');
url += ':' + (config.serverPort || 3000) + '/';

module.exports = {
  app: {
    title: pkg.title,
    version: pkg.version,
    description: pkg.description,
    url: url
  },

  ip: config.ip || '0.0.0.0',
  port: config.serverPort || 3000,

  isDevMode: config.nodeEnv === 'development',
  isTestMode: config.nodeEnv === 'test',
  isProductionMode: config.nodeEnv === 'production',

  sessions: {
    cookie: {
      maxAge: 7 * 24 * (60 * 60 * 1000),
      httpOnly: true,
      secure: config.isSecure
    },

    name: pkg.name + '-sessionId',
    collection: 'sessions'
  },

  db: {
    uri: config.dbAddr,
    options: {
      user: config.dbUser || '',
      pass: config.dbPassword || '',
      keepAlive: 1
    }
  },

  paypal: {
    clientId: config.paypalClientId,
    secret: config.paypalSecret
  },

  authKeys: {
    google: {
      clientID: null,
      clientSecret: null
    },

    facebook: {
      clientID: null,
      clientSecret: null
    },

    github: {
      clientID: null,
      clientSecret: null
    },

    twitter: {
      clientID: null,
      clientSecret: null
    }
  },

  cacheTimeout: 5 * 60,

  features: {
    disableSignUp: false,
    verificationRequired: true
  },

  logging: {
    console: {
      level: 'debug'
    },

    file: {
      enabled: true,
      path: path.join(global.rootPath, 'logs'),
      level: 'info',
      json: false,
      exceptionFile: true
    },
  },
  mailer: {
    enabled: false,
    from: config.notificationEmail,
    transport: 'smtp',
    smtp: {
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: '',
        pass: ''
      }
    }
  },
  agendaTimer: 'one minute'
};