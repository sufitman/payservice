let paypal = require('paypal-rest-sdk');
let uuid = require('node-uuid');
let config = require('../../config');
let logger = require('../logger');

module.exports = class CreditCard {
  constructor(options) {
    paypal.configure({
      'mode': options.mode,
      'client_id': options.clientId,
      'client_secret': options.secret
    });
    this.card = {
      type: options.type,
      number: options.number,
      expire_month: options.expire_month,
      expire_year: options.expire_year,
      first_name: options.first_name,
      payer_id: uuid.v4()
    };
  }

  create(data, callback) {
    paypal.credit_card.create(this.card, function(error, credit_card){
      if (error) {
        logger.error(error);
      } else {
        let card_data = {
          intent: data.intent,
          payer: {
            payment_method: data.payer.payment_method,
            funding_instruments: [{
              credit_card_token: {
                credit_card_id: credit_card.id,
                payer_id: credit_card.payer_id
              }
            }]
          },
          transactions: data.transactions
        };

        paypal.payment.create(card_data, callback);
      }
    });
  }

  delete(id) {
    paypal.credit_card.delete(id);
  }
};