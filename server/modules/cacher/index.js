let _ 				= require('lodash');

let MemoryCacher 	= require('./cacher-memory');

module.exports = function(prefix, ttl) {
  return new MemoryCacher(prefix, ttl);
};
