let logger = require('./logger');
let config = require('../config/base');
let C = require('./constants');

let _ = require('lodash');
let tokgen = require('../libs/tokgen');
let fakerator = require('fakerator')();

let User = require('../models/user');

module.exports = function() {
  /**
	 * Create default `admin` and `test` users
	 */

  return User.find({}).exec().then((docs) => {
    if (docs.length === 0) {
      logger.warn('Load default Users to DB...');

      let users = [];

      let admin = new User({
        fullName: 'Administrator',
        email: 'admin@example.com',
        username: 'admin',
        password: '1234567',
        provider: 'local',
        roles: [C.ROLE_ADMIN, C.ROLE_USER],
        verified: true
      });
      users.push(admin.save());

      let test = new User({
        fullName: 'Test User',
        email: 'test@example.com',
        username: 'test',
        password: '1234567',
        provider: 'local',
        roles: [C.ROLE_USER],
        verified: true,
        apiKey: tokgen()
      });
      users.push(test.save());

      return Promise.all(users)
        .then(() => {
          if (!config.isProductionMode) {
            return Promise.all(_.times(10, () => {
              let fakeUser = fakerator.entity.user();
              let user = new User({
                fullName: fakeUser.firstName + ' ' + fakeUser.lastName,
                email: fakeUser.email,
                username: fakeUser.userName,
                password: fakeUser.password,
                provider: 'local',
                roles: [C.ROLE_USER],
                verified: true
              });
              users.push(user.save());
            }));
          }
        })
        .then(() => {
          logger.warn('Default users created!');
        });
    }
  }).catch((err) => {
    logger.error(err);
  }).then(() => {
    logger.debug('Seeding done!');
  });	
};
