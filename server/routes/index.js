let connection = require('mongoose').connection;
module.exports = function(app) {
  // Index page
  app.get('/', function(req, res) {
    if (req.user) {
      res.render('main', {
        user: req.user
      });
    } else {
      res.redirect('login');
    }
  });

  // Handle account routes
  require('./account')(app, connection);

  // Handle Auth routes
  require('./auth')(app, connection);

  // Load services routes
  //require("../applogic/routeHandlers")(app, connection);
  let services = require('../modules/services');
  services.registerRoutes(app, connection);

  // Handle errors
  require('./errors')(app, connection);
};
