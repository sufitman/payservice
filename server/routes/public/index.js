let config = require('../../config');
let bodyParser = require('body-parser');
let CreditCard = require('../../modules/paypal');
let logger = require('../../modules/logger');
let Product = require('../../applogic/modules/products/models/product');
let Payment = require('../../applogic/modules/payments/models/payment');

let Form = require('../../applogic/modules/forms/models/form');
let User = require('../../models/user');

let HashIds = require('hashids');

module.exports = function(app) {
  app.use(bodyParser.urlencoded({
    extended: true,
    // limit: config.contentMaxLength * 2
  }));
  app.use( bodyParser.json() );

  // Payment form
  app.get('/payment-form', (req, res) => {
    res.render('public/error', { text: 'Form not found' });
  });

  app.get('/payment-status/:status', (req, res) => {
    res.send(req.params.status);
  });

  app.get('/payment-form/:code', (req, res) => {
    let hashids = new HashIds('forms' + config.hashSecret, 10);
    Form.findOne({ _id: hashids.decodeHex(req.params.code) }, [
      '-createdAt',
      '-updatedAt',
      '-code',
      '-id',
    ], (err, form) => {
      if (err || !form) {
        return res.render('public/error', {
          text: 'Form not found'
        });
      }
      let hashids = new HashIds('products' + config.hashSecret, 10);
      form.products.forEach((part, index, array) => array[index] = hashids.decodeHex(part));
      Product.find({ _id: { $in: form.products } }, [
        '-userCode',
        '-createdAt',
        '-updatedAt',
        '-id',
        '-description'
      ], (err, docs) => {
        if (err || !docs) {
          return res.render('public/error', {
            text: 'Products not found'
          });
        }

        res.render('public/payment-form', {
          id: req.params.code,
          url: config.app.url + 'payment-form',
          products: docs,
          form: form
        });
      });
    });
  });

  app.post('/payment-form', (req, res) => {
    let hashids = new HashIds('forms' + config.hashSecret, 10);
    Form.findOne({ _id: hashids.decodeHex(req.body.form_code) }, (err, form) => {
      if (err || !form) {
        return res.redirect(config.app.url + 'payment-status/error');
      }
      let hashids = new HashIds('products' + config.hashSecret, 10);
      Product.findOne({ _id: hashids.decodeHex(req.body.product_code) }, (e, product) => {
        if (err || !product) {
          return res.redirect(config.app.url + 'payment-status/error');
        }
        let payment_data = {
          to: form.userCode,
          name: req.body.fullname,
          email: req.body.email,
          amount: product.price,
          product: product.name,
          isMonthly: req.body.is_monthly === 'on'
        };
        // some logic for monthly payments
        // if (req.body.is_monthly) {
        // }

        payment_data.card = {
          no: req.body.card_no,
          exp_m: req.body.card_exp_month,
          exp_y: req.body.card_exp_year,
          cvc: req.body.card_cvc,
          zip: req.body.card_zip,
        };

        let hashids = new HashIds('users' + config.hashSecret, 10);
        User.findOne({ _id: hashids.decodeHex(req.body.to) }, (err, user) => {
          let card = new CreditCard({
            type: 'visa', // @TODO unhardcode it
            number: payment_data.card .no,
            expire_month: payment_data.card .exp_m,
            expire_year: payment_data.card .exp_y,
            first_name: payment_data.name,
            mode: user.paypalAccountType,
            clientId: user.paypalClientId,
            secret: user.paypalSecret,
          });
          card.create({
            intent: 'sale', //@TODO remove hardcoding
            payer: {
              payment_method: 'credit_card',
            },
            transactions: [{
              amount: {
                total: payment_data.amount,
                currency: 'USD', //@TODO remove hardcoding
                details: {
                  subtotal: payment_data.amount,
                  tax: 0,
                  shipping: 0
                }
              },
              description: 'Payment for ...'
            }]
          }, function (error, payment) {
            // @TODO add flash
            if (error) {
              logger.error(error);
            } else {
              // @TODO add some of the paypal's payment fields
              let payment = new Payment(payment_data);
              payment.save().then((doc) => {
                return res.redirect(res.redirect(config.app.url + 'payment-status/success'));
              });
            }
          });
        });
      });
    });
  });
};
