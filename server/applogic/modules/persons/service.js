let C 	 		= require('../../../modules/constants');

let _			= require('lodash');

let User 		= require('./models/user');

module.exports = {
  settings: {
    name: 'persons',
    version: 1,
    namespace: 'persons',
    rest: true,
    ws: true,
    permission: C.PERM_LOGGEDIN,
    role: 'user',
    collection: User,

    modelPropFilter: 'code username fullName avatar lastLogin roles paypalClientId paypalSecret paypalAccountType'
  },
	
  actions: {
    get: {
      cache: true,
      handler(ctx) {
        ctx.assertModelIsExist(ctx.t('app:UserNotFound'));
        return Promise.resolve(ctx.model);
      }
    }
  },

  methods: {
  },

};
