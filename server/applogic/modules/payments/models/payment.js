let config    		= require('../../../../config');
let mongoose 		= require('mongoose');
let Schema 			= mongoose.Schema;
let HashIds = require('hashids');
let hashids = new HashIds('payments' + config.hashSecret, 10);
let autoIncrement 	= require('mongoose-auto-increment');

let schemaOptions = {
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
};

let PaymentSchema = new Schema({
  to: {
    type: String,
    required: 'User to transfer is not found',
    ref: 'User'
  },
  name: {
    type: String,
    required: 'Please fill in your full name',
  },
  email: {
    type: String,
    required: 'Please fill in your email',
  },
  product: {
    type: String
  },
  amount: {
    type: Number,
    required: 'Please fill in amount to transfer',
  },
  isMonthly: {
    type: Boolean,
    default: false
  },
  card: {
    no: {
      type:Number,
      required: 'Please fill in the field'
    },
    exp_m:{
      type: Number,
      required: 'Please fill in the field'
    },
    exp_y:{
      type: Number,
      required: 'Please fill in the field'
    },
    cvc: {
      type: Number,
      required: 'Please fill in the field'
    },
    zip: {
      type: Number,
      required: 'Please fill in the field'
    }
  }
}, schemaOptions);


PaymentSchema.plugin(autoIncrement.plugin, {
  model: 'Payment',
  startAt: 1
});

PaymentSchema.methods.encodeID = function() {
  return hashids.encodeHex(this._id);
};

PaymentSchema.methods.decodeID = function(code) {
  return hashids.decodeHex(code);
};

let Payment = mongoose.model('Payment', PaymentSchema);

module.exports = Payment;
