let C 	 		= require('../../../modules/constants');

let _			= require('lodash');

let Payment 		= require('./models/payment');

module.exports = {
  settings: {
    name: 'payments',
    version: 1,
    namespace: 'payments',
    rest: true,
    ws: true,
    permission: C.PERM_LOGGEDIN,
    role: 'user',
    collection: Payment,
    modelPropFilter: 'createdAt updatedAt name email amount product isMonthly'
  },
	
  actions: {
    find: {
      cache: false,
      handler(ctx) {
        let filter = {
          to: ctx.user.code
        };

        let query = Payment.find(filter);
        return ctx.queryPageSort(query).exec().then( (docs) => {
          return this.toJSON(docs);
        });
      }
    },

    // return a model by ID
    get: {
      cache: false,
      handler(ctx) {
        ctx.assertModelIsExist(ctx.t('app:PaymentNotFound'));
        return Promise.resolve(ctx.model);
      }
    },

    create(ctx) {
      this.validateParams(ctx, true);
      let payment = new Payment({
        to: ctx.params.to,
        name: ctx.params.name,
        email: ctx.params.email,
        product: ctx.params.product,
        card: ctx.params.card,
        isMonthly: ctx.params.isMonthly
      });

      return payment.save()
        .then((doc) => {
          return this.toJSON(doc);
        })
        .then((json) => {
          return this.populateModels(json);
        })
        .then((json) => {
          this.notifyModelChanges(ctx, 'created', json);
          return json;
        });	
    },

    update(ctx) {
      ctx.assertModelIsExist(ctx.t('app:PaymentNotFound'));
      this.validateParams(ctx);

      return this.collection.findById(ctx.modelID).exec()
        .then((doc) => {

          doc.to = ctx.params.to;
          doc.name = ctx.params.name;
          doc.email = ctx.params.email;
          doc.amount = ctx.params.amount;
          doc.product = ctx.params.product;
          doc.isMonthly = ctx.params.isMonthly;
          doc.card = ctx.params.card;

          return doc.save();
        })
        .then((doc) => {
          return this.toJSON(doc);
        })
        .then((json) => {
          return this.populateModels(json);
        })
        .then((json) => {
          this.notifyModelChanges(ctx, 'updated', json);
          return json;
        });								
    },
  },
	
  methods: {
    /**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 * 
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
    validateParams(ctx, strictMode) {
      // @TODO validate object "payments"
      if (ctx.hasValidationErrors())
        throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);			
    }
  },	

  init(ctx) {
    // Fired when start the service
  },

  socket: {
    afterConnection(socket, io) {
      // Fired when a new client connected via websocket
    }
  }

};