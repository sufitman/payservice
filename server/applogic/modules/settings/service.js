let C 	 		= require('../../../modules/constants');

let _			= require('lodash');

let User 		= require('./models/user');

module.exports = {
  settings: {
    name: 'settings',
    version: 1,
    namespace: 'settings',
    rest: true,
    ws: true,
    permission: C.PERM_LOGGEDIN,
    role: 'user',
    collection: User,
    modelPropFilter: 'paypalClientId paypalSecret paypalAccountType'
  },
	
  actions: {
    update(ctx) {
      ctx.assertModelIsExist(ctx.t('app:UserNotFound'));
      this.validateParams(ctx);

      return this.collection.findById(ctx.modelID).exec()
        .then((doc) => {

          doc.paypalClientId = ctx.params.paypalClientId;
          doc.paypalSecret = ctx.params.paypalSecret;
          doc.paypalAccountType = ctx.params.paypalAccountType;

          return doc.save();
        })
        .then((doc) => {
          return this.toJSON(doc);
        })
        .then((json) => {
          return this.populateModels(json);
        })
        .then((json) => {
          this.notifyModelChanges(ctx, 'updated', json);
          return json;
        });								
    },

  },
	
  methods: {
    /**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 * 
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
    validateParams(ctx, strictMode) {
      // @TODO validate object "User"
      if (ctx.hasValidationErrors())
        throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);			
    }
  },	

  init(ctx) {
    // Fired when start the service
  },

  socket: {
    afterConnection(socket, io) {
      // Fired when a new client connected via websocket
    }
  }

};