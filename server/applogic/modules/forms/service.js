let C 	 		= require('../../../modules/constants');

let _			= require('lodash');

let Form 		= require('./models/form');

module.exports = {
  settings: {
    name: 'forms',
    version: 1,
    namespace: 'forms',
    rest: true,
    ws: true,
    permission: C.PERM_LOGGEDIN,
    role: 'user',
    collection: Form,
		
    modelPropFilter: 'code products createdAt updatedAt width widthMeasure height heightMeasure'
  },
	
  actions: {
    find: {
      cache: true,
      handler(ctx) {
        let filter = {
          userCode: ctx.user.code
        };

        let query = Form.find(filter);
        return ctx.queryPageSort(query).exec().then( (docs) => {
          return this.toJSON(docs);
        });
      }
    },

    // return a model by ID
    get: {
      cache: true,
      handler(ctx) {
        ctx.assertModelIsExist(ctx.t('app:FormNotFound'));
        return Promise.resolve(ctx.model);
      }
    },

    create(ctx) {
      this.validateParams(ctx, true);
      let form = new Form({
        products: ctx.params.products,
        userCode: ctx.params.userCode,
        width: ctx.params.width,
        widthMeasure: ctx.params.widthMeasure,
        height: ctx.params.height,
        heightMeasure: ctx.params.heightMeasure
      });

      return form.save()
        .then((doc) => {
          return this.toJSON(doc);
        })
        .then((json) => {
          return this.populateModels(json);
        })
        .then((json) => {
          this.notifyModelChanges(ctx, 'created', json);
          return json;
        });	
    },

    update(ctx) {
      ctx.assertModelIsExist(ctx.t('app:FormNotFound'));
      this.validateParams(ctx);

      return this.collection.findById(ctx.modelID).exec()
        .then((doc) => {

          doc.products = ctx.params.products;
          doc.width = ctx.params.width;
          doc.widthMeasure = ctx.params.widthMeasure;
          doc.height = ctx.params.height;
          doc.heightMeasure = ctx.params.heightMeasure;

          return doc.save();
        })
        .then((doc) => {
          return this.toJSON(doc);
        })
        .then((json) => {
          return this.populateModels(json);
        })
        .then((json) => {
          this.notifyModelChanges(ctx, 'updated', json);
          return json;
        });								
    },

    remove(ctx) {
      ctx.assertModelIsExist(ctx.t('app:FormNotFound'));

      return Form.remove({ _id: ctx.modelID })
        .then(() => {
          return ctx.model;
        })
        .then((json) => {
          this.notifyModelChanges(ctx, 'removed', json);
          return json;
        });		
    }

  },
	
  methods: {
    /**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 * 
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
    validateParams(ctx, strictMode) {
      // @TODO validate object "products"
      if (ctx.hasValidationErrors())
        throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);			
    }
  },	

  init(ctx) {
    // Fired when start the service
  },

  socket: {
    afterConnection(socket, io) {
      // Fired when a new client connected via websocket
    }
  }

};