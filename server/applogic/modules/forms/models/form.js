let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let config = require('../../../../config');
let HashIds = require('hashids');
let hashids = new HashIds('forms' + config.hashSecret, 10);
let autoIncrement 	= require('mongoose-auto-increment');

let schemaOptions = {
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
};

let FormSchema = new Schema({
  products: [ String ],
  userCode: {
    type: String,
    required: 'User is not found',
    ref: 'User'
  },
  width: {
    type: Number
  },
  widthMeasure: {
    type: String
  },
  height: {
    type: Number
  },
  heightMeasure: {
    type: String
  }
}, schemaOptions);

FormSchema.virtual('code').get(function() {
  return this.encodeID();
});

FormSchema.plugin(autoIncrement.plugin, {
  model: 'Form',
  startAt: 1
});

FormSchema.methods.encodeID = function() {
  return hashids.encodeHex(this._id);
};

FormSchema.methods.decodeID = function(code) {
  return hashids.decodeHex(code);
};

let Form = mongoose.model('Form', FormSchema);

module.exports = Form;
