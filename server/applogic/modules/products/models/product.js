let mongoose = require('mongoose');
let Schema = mongoose.Schema;
let config = require('../../../../config');
let HashIds = require('hashids');
let hashids = new HashIds('products' + config.hashSecret, 10);
let autoIncrement 	= require('mongoose-auto-increment');

let schemaOptions = {
  timestamps: true,
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
};

let productschema = new Schema({
  userCode: {
    type: String,
    required: 'User is not found',
    ref: 'User'
  },
  name: {
    type: String,
    required: 'Name is required'
  },
  price: {
    type: Number,
    required: 'Price is required'
  },
  currency: {
    type: String,
    required: 'Currency is required'
  },
  description: {
    type: String
  }
}, schemaOptions);

productschema.virtual('code').get(function() {
  return this.encodeID();
});

productschema.plugin(autoIncrement.plugin, {
  model: 'Product',
  startAt: 1
});

productschema.methods.encodeID = function() {
  return hashids.encodeHex(this._id);
};

productschema.methods.decodeID = function(code) {
  return hashids.decodeHex(code);
};

let Product = mongoose.model('Product', productschema);

module.exports = Product;
