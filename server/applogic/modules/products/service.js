let C 	 		= require('../../../modules/constants');

let _			= require('lodash');

let Product 		= require('./models/product');

module.exports = {
  settings: {
    name: 'products',
    version: 1,
    namespace: 'products',
    rest: true,
    ws: true,
    permission: C.PERM_LOGGEDIN,
    role: 'user',
    collection: Product,
    modelPropFilter: 'code createdAt updatedAt name price currency description'
  },
	
  actions: {
    find: {
      cache: true,
      handler(ctx) {
        let filter = {
          userCode: ctx.user.code
        };

        let query = Product.find(filter);
        return ctx.queryPageSort(query).exec().then( (docs) => {
          return this.toJSON(docs);
        });
      }
    },

    // return a model by ID
    get: {
      cache: true,
      handler(ctx) {
        ctx.assertModelIsExist(ctx.t('app:ProductNotFound'));
        return Promise.resolve(ctx.model);
      }
    },

    create(ctx) {
      this.validateParams(ctx, true);
      let product = new Product({
        userCode: ctx.params.userCode,
        name: ctx.params.name,
        price: ctx.params.price,
        currency: ctx.params.currency,
        description: ctx.params.description
      });

      return product.save()
        .then((doc) => {
          return this.toJSON(doc);
        })
        .then((json) => {
          return this.populateModels(json);
        })
        .then((json) => {
          this.notifyModelChanges(ctx, 'created', json);
          return json;
        });	
    },

    update(ctx) {
      ctx.assertModelIsExist(ctx.t('app:ProductNotFound'));
      this.validateParams(ctx);

      return this.collection.findById(ctx.modelID).exec()
        .then((doc) => {

          doc.name = ctx.params.name;
          doc.price = ctx.params.price;
          doc.currency = ctx.params.currency;
          doc.description = ctx.params.description;

          return doc.save();
        })
        .then((doc) => {
          return this.toJSON(doc);
        })
        .then((json) => {
          return this.populateModels(json);
        })
        .then((json) => {
          this.notifyModelChanges(ctx, 'updated', json);
          return json;
        });								
    },

    // remove(ctx) {
    //   ctx.assertModelIsExist(ctx.t('app:ProductNotFound'));
    //
    //   return Product.remove({ _id: ctx.modelID })
    //     .then(() => {
    //       return ctx.model;
    //     })
    //     .then((json) => {
    //       this.notifyModelChanges(ctx, 'removed', json);
    //       return json;
    //     });		
    // }

  },
	
  methods: {
    /**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 * 
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
    validateParams(ctx, strictMode) {
      // @TODO validate object "products"
      if (ctx.hasValidationErrors())
        throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);			
    }
  },	

  init(ctx) {
    // Fired when start the service
  },

  socket: {
    afterConnection(socket, io) {
      // Fired when a new client connected via websocket
    }
  }

};